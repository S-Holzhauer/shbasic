#' Timeline figure with convergence end information.
#' 
#' @param sip 
#' @param datas 
#' @param figureInfo 
#' @param parameterData 
#' @param ... 
#' @return figure
#' 
#' @author Sascha Holzhauer
#' @export
shbasic_fig_convergence_end <- function(sip, datas, figureInfo, parameterData = NULL, ...) {
	
	futile.logger::flog.info("shbasic_fig_convergence_end: NOT YET CONVERTED!",
				name = "shbasic.shbasic_figure_convergence.r")
		
	###############################
	#### Parameter Data         ###
	###############################
	runIDs <- my.runIdRangeFromRunConfig(runConfig)
	
	# set lix$numFigs if not defined before:
	if (is.null(lix$numFigs)) 	
		lix$numFigs <- as.numeric(length(unique(figureInfo[figureInfo[, infoCol[['type']]] %in% c(1,4) &
										figureInfo[, infoCol[['figure']]] > 0, infoCol[['figure']]])))
	
	if (lix$splitFigs == 0) {
		lix$filename	<<- paste(lix$fn_prefix, "-", runConfig$startRunID[1], "-GGplot-", lix$fn_postfix, sep="")
		my.initFigFile()
		par(mfrow=c(lix$numFigs, 1))
	}
	
	
	
	###############################
	#### Drawing figures        ###
	###############################
	
	print("Plot now...")

	for (j in sort(unique(as.numeric(figureInfo[figureInfo[, infoCol[['type']]] %in% c(1,4), infoCol[['figure']]])))[
			which(unique(figureInfo[figureInfo[, infoCol[['type']]] %in% c(1,4), infoCol[['figure']]]) != 0)]){
		
		# TODO check only x/y lmits of column that apear in the current plot!
		
		library(R.oo)
		if (length(datas[[1]]) == 0) {
			throw("Data is empty!")
		}
		
		if ((!is.null(lix$omitTitle)) && lix$omitTitle > 0) {
			mainText = NULL
		} else {
			mainText = paste(lix$titlePre, paste(": ", my.subLabel(substituteLabels, figureInfo[figureInfo[,infoCol[['yLabel']]]== j, infoCol[['name']]][1])
							, " for runIDs ", my.makeCondensedIdSet(runIDs), sep =""), " ", lix$titleAdd, sep="")
		}
		
		### <-- GG-PLOT ###

		theme_set(theme_phd())
		gg_plot <- ggplot()
		
		
		### GG-PLOT --> ###
	
		# define a color for each runID
		otherColors = my.getColors(length(runIDs))
		labelColor <- c()
		labelStyle <- c()
		labels <- c()
		
		print("Points...")
		
		# determine limits for colour values:
		colLimits <- list()
		colLimits$min = Inf
		colLimits$max = -Inf
	
			
		if (length(figureInfo[figureInfo[, infoCol[['colour']]] == j & figureInfo[, infoCol[['type']]] %in% c(1,4), infoCol[['name']]]) == 0) {
			throw("No colour column specified!")
		}
		colLimits$min <- min(colLimits$min, 
				min(datas[,figureInfo[figureInfo[, infoCol[['colour']]] == j & figureInfo[, infoCol[['type']]] %in% c(1,4), infoCol[['name']]]]))
		colLimits$max <- max(colLimits$max, 
				max(datas[,figureInfo[figureInfo[, infoCol[['colour']]] == j & figureInfo[, infoCol[['type']]] %in% c(1,4), infoCol[['name']]]]))
		
		for (i in 1:sum(figureInfo[, infoCol[['figure']]]== j & figureInfo[, infoCol[['type']]] %in% c(1,4))) {
			if (length(figureInfo[figureInfo[,infoCol[['yLabel']]] == -1,]) > 0) {
				runIdText = paste("[",my.substitute(figureInfo[figureInfo[,infoCol[['yLabel']]] == -1,infoCol[['name']]]), " = ", parameterData[parameterData[,"runID"] == runConfig$startRunID[1],
						figureInfo[figureInfo[,infoCol[['yLabel']]] == -1,infoCol[['name']]]][1],"]", sep="")
			} else {
				runIdText = runConfig$startRunID
			}
			
			### <-- GG-PLOT ###
			if ((!is.null(lix$oderByRandomSeed)) && lix$oderByRandomSeed > 0) {
				# order unique runIDs along RandomSeed:
				datas <- datas[order(datas["de.cesr.sonomode.param.RandomPa:RANDOM_SEED"], datas["runID"]),]		
				uniqueRunIDs <- unique(datas[,c("runID","de.cesr.sonomode.param.RandomPa:RANDOM_SEED")])
				uniqueRunIDs["order"] <-order(uniqueRunIDs[,"de.cesr.sonomode.param.RandomPa:RANDOM_SEED"])
				
				datas["order"] <- uniqueRunIDs[match(datas[,"runID"],uniqueRunIDs[,"runID"]), "order"]
			} else {
				datas["order"] <- datas[,"runID"]
			}
			
			gg_plot <- gg_plot +
				layer(
					data = data.frame(x = factor(datas$order)
					, y = datas[,figureInfo[figureInfo[, infoCol[['figure']]] == j & 
											figureInfo[, infoCol[['type']]]==1, infoCol[['name']]][i]]
					,col = datas[,figureInfo[figureInfo[, infoCol[['colour']]] == j & 
											figureInfo[, infoCol[['type']]]==1, infoCol[['name']]][i]]
					,Amount	= datas[,figureInfo[figureInfo[, infoCol[['colour']]] == -j & 
											figureInfo[, infoCol[['type']]]==1, infoCol[['name']]][i]])
					,mapping = aes(x,y, colour = col, size = Amount)
					,geom="point", stat = "identity"
				)	+ 
				labs(title = lix$titlePre) + 
				theme(axis.text.x=element_text(angle=70, hjust=1.0, vjust=1, size=lix$xaxisSize))
		
			### GG-PLOT --> ###
			
			# colors per runID, style per data row
			labelColor[i] = otherColors[i]
			labelStyle[i] = lineStyles[i]
			labels[i] = 
					paste(runIdText, ": ", my.subLabel(substituteLabels, figureInfo[figureInfo[, infoCol[['type']]] > 0 &
													figureInfo[, infoCol[['figure']]] == j, 
											infoCol[['name']]][i]), sep="")
		}
		
		parameterData <<- parameterData
		gg_plot <- gg_plot + 
			scale_x_discrete(my.subLabel(substituteLabels, figureInfo[figureInfo[,infoCol[["xLabel"]]]== 
					if (length(figureInfo[figureInfo[, infoCol[['xLabel']]]==j, 1]) > 0) j else 1, infoCol[["name"]]][1]),
				breaks = unique(datas$order),
				labels = vapply(unique(datas$runID), makeXLabel, c(""))) + 
			scale_y_continuous(
				my.subLabel(substituteLabels, figureInfo[figureInfo[,infoCol[['yLabel']]]== j, infoCol[['name']]][1]),
				limits= c( max(limits$minY, min(datas[,figureInfo[figureInfo[, infoCol[['figure']]] == j & 
					figureInfo[, infoCol[['type']]] %in% c(1,4), infoCol[['name']]]])), 
					min(limits$maxY, max(datas[,figureInfo[figureInfo[, infoCol[['figure']]] == j & 
					figureInfo[, infoCol[['type']]] %in% c(1,4), infoCol[['name']]]])))) + 
			coord_cartesian() + 
			# guide = guide_colourbar(barheight = 4)
			scale_colour_gradient(guide = guide_legend(keyheight = 1.4), name= my.subLabel(substituteLabels, figureInfo[figureInfo[,infoCol[["colour"]]]== 
					if (length(figureInfo[figureInfo[, infoCol[['colour']]]==j, 1]) > 0) j else 1, infoCol[["name"]]][1]),
					limits = lix$colValLimits, low="green", high="red") + 
			scale_size(name= my.subLabel(substituteLabels, figureInfo[figureInfo[,infoCol[["colour"]]]== 
					if (length(figureInfo[figureInfo[, infoCol[['colour']]]==-j, 1]) > 0) -j else -1, infoCol[["name"]]][1]))

		if (lix$splitFigs > 0) {
			print("Split GGplot figures...")
			# specify number of figures:
			if (j > 1) dev.off()
			
			if (lix$numFigs > 1 | is.null(lix$filename)) {
				lix$filename	<<- paste(lix$fn_prefix, "-", runConfig$startRunID[1], "-GGplot-", lix$fn_postfix,"_",j, sep="")
			}
			my.initFigFile()
			par(mfrow=c(1, 1))
			print(gg_plot)
		} else {
			print(gg_plot)
		}
		### GG-PLOT --> ###
	}
	
	if (lix$sweave <= 0) {
		dev.off()
	}
}