---
title: "Introduction to shbasic"
author: "Sascha Holzhauer"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Introduction to shbasic}
  %\VignetteEngine{knitr::rmarkdown}
  %\usepackage[utf8]{inputenc}
---

# Simulation Parameter Objects

```{r, eval=TRUE}
shbasic::shbasic_param_getDefaultSip()
```

# Label and Value substitution

The package provides a number of function to substitute mainly string in data and parameters.
Some appliances are obvious:

 * Substitute strange abbreviations or acronyms in variables or filenames by more meaningful alternatives.
 * Substitute lengthy strings (often needed for unambiguousness in simulation environments) shorter
   equivalents (which are fine in the context)
 * Translate labels to another language
 
The functions start with `sh_basic_substitute`.
 
```{r, eval=TRUE}
sip <- shbasic::shbasic_param_getDefaultSip()
sip$sub$subs <- data.frame("col_Y", "yellow", stringsAsFactors = FALSE)
cat("The colour is ", shbasic::shbasic_substitute(sip, "col_Y"))
```

# Retrieve data from DB

## Data Definition Object

Data definition objects consist usually of 12 columns:

Column Name     | Description
--------------- | -------------
table           | MySQL tablename
column name     | MySQL column name
type            | 
figure          |
pre	            |
milieu-specific |
x               |
y               |
where           |
sql             |
plotType        |
color           |

# Store data in DB

## Generating Parameter Definitions

### Agent Locations

```{r, eval=FALSE}
sip <- shbasic::shbasic_param_getDefaultSip()
sip$paramgen$loc$locsd <- 30
shbasic::shbasic_paramgen_location_villages(sip, agentid = 1, numagents = 25000)
```

# Figures

## Initialisation of Figures

```{r, eval=FALSE}
sip <- shbasic::shbasic_param_getDefaultSip()
sip$fig$length = 1000
sip$fig$height = 700
sip$fig$resfactor = 3
output_visualise_initFigure(sip, outdir = "./", filename = "Figure.png", ensurePath = TRUE)
```

Unless stated otherwise, any visualising functions return ggplot objects that need to be printed, possibly by using shbasic_visualise_multiplot and initialising the device beforehand, or output_visualise_splitFigureGgplotList which included initialising the device. 