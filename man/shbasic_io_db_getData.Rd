% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/shbasic_io_db.R
\name{shbasic_io_db_getData}
\alias{shbasic_io_db_getData}
\title{Retrieve data for datadef and runconfig from configured DB}
\usage{
shbasic_io_db_getData(sip, runconfig, datadef, retrievedtypes = c(1, 4,
  5, 10))
}
\arguments{
\item{sip}{SImulation Parameters (see \code{\link{shbasic_param_getDefaultSip}})
Considered parameters:
\itemize{
    \item sip$db$datadefheader
    \item sip$db$tables$runinfo
 \item sip$db$runidcolumn
 \item sip$debug$db
 \item see \code{\link{shbasic_io_db_createSQL}}
 \item see \code{\link{shbasic_io_db_getResult}}
}}

\item{runconfig}{Run configuration (see \code{\link{shbasic_param_getDefaultRunconfig}})}

\item{datadef}{Data definition. Defines which data is retrieved from database.}

\item{retrievedtypes}{Refers to the assigned types in datadef}
}
\value{
list of requested data (one entry per runs definitions in runconfig) as matrix
}
\description{
Retrieve data for datadef and runconfig from configured DB
}
\examples{
sip <- shbasic::shbasic_param_getDefaultSip()
runconfig <- shbasic::shbasic_param_getDefaultRunconfig()
runconfig$start_run_id[1]	= 1
runconfig$end_run_id[1] 	= 2
runconfig$name[1]			= "ExampleA"

datadef <- matrix(
	# table-----------------column name ----------------------type--fig-pre-milieu-x--y--where--sql------------plotType-color
	c(	sip$db$tables$runinfo, "runID",							1,	0, 	0,	0,	   1, 0, "'true'","",  				"l", 0,
		sip$db$tables$results[1], "Net-Mod-InfoMap",			1,	0, 	0,	0,	   0, 0, "'network=\\"SocialNetwork\\"'","",				"l", 0,
		sip$db$tables$results[1], "Net-Mod-InfoMapUndirected",	1,	1,	0, 	0,	   0, 0, "'true'","",					"l", 0,
		sip$db$tables$results[1], "R_N-Cl-overall",				1,	0,	0, 	0,	   0, 0, "'true'","",					"l", 0
	),byrow=TRUE, ncol=length(sip$db$datadefheader))
data <- shbasic_io_db_getDataRunidLinked(sip, runconfig, datadef)
}
\author{
Sascha Holzhauer
}
