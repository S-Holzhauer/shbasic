shbasic
=============

The package provides some basic functionality. Feature include:

 * Simulation-Parameter framework (SIP)
 
 * Initialisation of random agent locations for spatial ABM
 
 * Visualising agent locations
 
 * SIP-based MySQL database communication
 
 * SIP-based storage and loading
 
 * Substitution in text files
 
 * Ensuring path (create if non-existing)
 
 * String substitution
 
 * Load or Save (load data if existing, generate and save otherwise)
 
# Installation

``devtools::install_bitbucket("S-Holzhauer/shbasic@master")``


# First Steps

Start by reading the introduction vignette:  
``browseVignettes("shbasic")``


# Contact

If you're interested in using the package, or for discussions, questions and feature requests don't hesitate to contact
sascha.holzhauer@uni-kassel.de
